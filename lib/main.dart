import 'package:flutter/material.dart';
import 'package:sliver_animation/album.dart';
import 'package:vector_math/vector_math_64.dart' as vector;

void main() {
  runApp(MyApp());
}

const _colorHeader = Color(0xFFECECEA);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              delegate: _MyVinylDiscHeader(),
              pinned: true,
            ),
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.all(20.0),
                child: Text(currentAlbum.description),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

const _maxHeaderExtend = 350.0;
const _minHeaderExtend = 100.0;
const _maxImageSize = 160.0;
const _minImageSize = 60.0;
const _maxleftMarginDisc = 200.0;
const _minleftMarginDisc = 35.0;
const _maxTitleSize = 25.0;
const _maxSubTitleSize = 18.0;
const _minTitleSize = 16.0;
const _minSubTitleSize = 12.0;

class _MyVinylDiscHeader extends SliverPersistentHeaderDelegate {
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final percent = shrinkOffset / _maxHeaderExtend;
    final size = MediaQuery.of(context).size;
    final currentImageSize = (_maxImageSize * (1 - percent)).clamp(
      _minImageSize,
      _maxImageSize,
    );
    final titleSize = (_maxTitleSize * (1 - percent)).clamp(
      _minTitleSize,
      _maxTitleSize,
    );
    final subTitleSize = (_maxSubTitleSize * (1 - percent)).clamp(
      _minSubTitleSize,
      _maxSubTitleSize,
    );
    final maxMargin = (size.width / 4);
    final textMovement = 50.0;
    final leftTextMargin = maxMargin + (textMovement * percent);
    final leftMarginDisc = (_maxleftMarginDisc * (1 - percent)).clamp(
      _minleftMarginDisc,
      _maxleftMarginDisc,
    );
    return Container(
      color: _colorHeader,
      child: Stack(
        children: [
          Positioned(
            top: 50.0,
            left: leftTextMargin,
            height: _maxImageSize,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  currentAlbum.artist,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: titleSize,
                    letterSpacing: -0.5,
                  ),
                ),
                Text(
                  currentAlbum.album,
                  style: TextStyle(
                    fontSize: subTitleSize,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 20.0,
            left: leftMarginDisc,
            height: currentImageSize - 20,
            child: Transform.rotate(
              angle: vector.radians(360 * percent),
              child: Image.asset(
                currentAlbum.imageDisc,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Positioned(
            bottom: 20.0,
            left: 35.0,
            height: currentImageSize,
            child: Image.asset(
              currentAlbum.imageAlbum,
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => _maxHeaderExtend;

  @override
  double get minExtent => _minHeaderExtend;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      false;
}
