class Album {
  const Album({
    required this.artist,
    required this.album,
    required this.imageAlbum,
    required this.imageDisc,
    required this.description,
  });

  final String artist;
  final String album;
  final String imageAlbum;
  final String imageDisc;
  final String description;
}

final currentAlbum = Album(
  artist: 'Dexter Morgan',
  album: 'Woman and man',
  imageAlbum: 'assets/images/art.jpg',
  imageDisc: 'assets/images/palette.png',
  description: r'''
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation. 
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation. 
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation. 
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation.
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation. 
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation. 
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation. 
      Art, also called (to distinguish it from other art forms) visual art, a visual object or experience consciously created through an expression of skill or imagination. 
      The term art encompasses diverse media such as painting, sculpture, printmaking, drawing, decorative arts, photography, and installation.
      ''',
);
